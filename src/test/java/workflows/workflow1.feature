@ignore
Feature: workflow 1

  Background:
#    * configure logPrettyRequest = true
#    * configure logPrettyResponse = true
    * print 'debug workflow 1'

  Scenario: workflow 1 : scenario 1
    * def unit1 = call read('../units/unit1.feature')
    * callonce read('../commons/sleep.feature') {timeout: 3000}
    * print "response:" + unit1.response.name
    * callonce read('../units/unit2.feature')

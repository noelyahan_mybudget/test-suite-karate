@ignore
Feature:

  Background:
#    todo wrap fail messages in json format
  * configure afterScenario = function(){ karate.log('*** MESSAGE:', karate.info.errorMessage); karate.call('../commons/log.feature') }
  * configure afterFeature = function() { karate.log('%%% AFTER FEATURE') }

  Scenario:
    * print title + " : " + "unit 1" + " : " + id
    * match 1 == 1
    * def Producer = Java.type('lib.Producer')
    * def producer = new Producer()
    * producer.Produce("custom event")

  Given def cat =
  """
{
  name: 'Billie',
  kittens: [
      { id: 23, name: 'Bob' },
      { id: 42, name: 'Wild' }
  ]
}
"""
#    https://github.com/intuit/karate/blob/master/karate-junit4/src/test/java/com/intuit/karate/junit4/demos/examples.feature
  Then match cat.kittens[0] == { name: 'Bob', id: 23 }
  * def response = { name: 'Bob', id: 23 }
#  call http service

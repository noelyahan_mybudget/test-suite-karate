@ignore
Feature:

  Background:
#    todo wrap fail messages in json format
    * configure afterScenario = function(){ karate.log('*** MESSAGE UNIT2:', karate.info.errorMessage); karate.call('../commons/log.feature') }
    * configure afterFeature = function() { karate.log('%%% MESSAGE UNIT2 AFTER FEATURE') }

  Scenario:
    * print title + " : " + "unit 2" + " : " + id
    * url 'https://jsonplaceholder.typicode.com/users/1'
    * method GET
    * status 200
    * print "response start"
    * print response
    * print "response end"
    * def name = "Romaguera-Crona"
    * match response ==
    """
    {
  "website": "hildegard.org",
  "address": {
    "zipcode": "92998-3874",
    "geo": {
      "lng": "81.1496",
      "lat": "-37.3159"
    },
    "suite": "Apt. 556",
    "city": "Gwenborough",
    "street": "Kulas Light"
  },
  "phone": "1-770-736-8031 x56442",
  "name": "Leanne Graham",
  "company": {
    "bs": "harness real-time e-markets",
    "catchPhrase": "Multi-layered client-server neural-net",
    "name": "#(name)"
  },
  "id": 1,
  "email": "Sincere@april.biz",
  "username": "Bret"
}
    """

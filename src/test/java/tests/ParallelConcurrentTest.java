package tests;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import org.junit.Test;
import static org.junit.Assert.*;


public class ParallelConcurrentTest {
    
    @Test
    public void testParallel() {
        System.out.println("----Running the parallel concurrent tests----");
        Results results = Runner.path("classpath:tests").tags("~@ignore").parallel(20);
        assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
    }

}
#@ignore
Feature: workflow 2 load

  Background:
#    * configure logPrettyRequest = true
#    * configure logPrettyResponse = true
    * print 'debug workflow 3 load'

  Scenario: workflow 2 : scenario 1 : load
    * def load = call read('../commons/load.feature')
    * def N = load.count(2, 'workflow 3')
    * call read('../workflows/workflow3.feature') N
    * print "debug workflow 3 load done"
